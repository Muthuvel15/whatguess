<?php

namespace App\Core;

use phpDocumentor\Reflection\Types\Self_;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    const ORDER_NAME = array("as"=>14,"roi"=>13,"reine"=>12,"valet"=>11,
                             "10"=>10,"9"=>9,"8"=>8,"7"=>7,
                              "6"=>6,"5"=>5,"4"=>4,"3"=>3,"2"=>2);

    const ORDER_COLOR = array("trèfle"=>1,"pique"=>2,"carreau"=>3,"coeur"=>4);
  /**
   * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
   */
  private $name;

  /**
   * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
   */
  private $color;

  /**
   * Card constructor.
   * @param string $name
   * @param string $color
   */
  public function __construct(string $name, string $color)
  {
    $this->name = $name;
    $this->color = $color;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

    public function getColor(): string
    {
        return $this->color;
    }


    /** définir une relation d'ordre entre instance de Card.
   *  Remarque : cette méthode n'est pas de portée d'instance
   *
   * @see https://www.php.net/manual/fr/function.usort.php
   *
   * @param $o1 Card
   * @param $o2 Card
   * @return int
   * <ul>
   *  <li> zéro si $o1 et $o2 sont considérés comme égaux </li>
   *  <li> -1 si $o1 est considéré inférieur à $o2</li>
    * <li> +1 si $o1 est considéré supérieur à $o2</li>
   * </ul>
   *
   */
    public static function cmp(Card $o1, Card $o2) : int
    {


        $o1Name = strtolower($o1->getName());
        $o2Name = strtolower($o2->getName());

        $o1Color = strtolower($o1->getColor());
        $o2Color = strtolower($o2->getColor());

        //Meme Noms & Meme Couleur


        if(($o1Name == $o2Name) && ($o1Color == $o2Color)){
            return 0;
        }
        //Meme Noms & Differentes couleur
        else if(($o1Name == $o2Name) && ($o1Color != $o2Color))
        {
            if(Card::ORDER_COLOR[$o1Color] > Card::ORDER_COLOR[$o2Color])
            {
                return +1;
            }
            else
            {
                return -1;
            }
            //Different Noms & Memes Couleur
        } else if (($o1Name != $o2Name) && ($o1Color == $o2Color)){
            if(Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name]){
                return +1;
            }else{
                return -1;
            }


            //Different Noms & different Couleur
        }else{
            if(Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name])
            {
                return +1;
            }else{
                return -1;
            }
        }
    }

    public static function cmp1(Card $o1, Card $o2) : int
    {


        $o1Name = strtolower($o1->getName());
        $o2Name = strtolower($o2->getName());

        $o1Color = strtolower($o1->getColor());
        $o2Color = strtolower($o2->getColor());

        //Meme Noms & Meme Couleur


        if(($o1Name == $o2Name) && ($o1Color == $o2Color)){
            return 0;
        }
        //Meme Noms & Differentes couleur
        else if(($o1Name == $o2Name) && ($o1Color != $o2Color))
        {
            if(Card::ORDER_COLOR[$o1Color] > Card::ORDER_COLOR[$o2Color])
            {
                return 1;
            }
            else
            {
                return 2;
            }
            //Different Noms & Memes Couleur
        } else if (($o1Name != $o2Name) && ($o1Color == $o2Color))
        {
            if(Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name])
            {
                return 3;
            }
            else
            {
                return 4;
            }
            //Different Noms & different Couleur
        }
        else
        {
            if((Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name]) && (Card::ORDER_COLOR[$o1Color] > Card::ORDER_COLOR[$o2Color]))
            {
                return 5;
            }
            else if((Card::ORDER_NAME[$o1Name] < Card::ORDER_NAME[$o2Name]) && (Card::ORDER_COLOR[$o1Color] < Card::ORDER_COLOR[$o2Color]))
            {
                return 6;
            }
            else if((Card::ORDER_NAME[$o1Name] < Card::ORDER_NAME[$o2Name]) && (Card::ORDER_COLOR[$o1Color] > Card::ORDER_COLOR[$o2Color]))
            {
                return 7;
            }
            else
            {
                return 8;
            }
        }
    }

  public function __toString() : string
  {
      return "Name: ".$this->name." Couleur: ".$this->color;
  }


     public static function creatCardCollection():array
    {
        $card = array();
        foreach (Card::ORDER_COLOR as $colorName => $colorValue)
        {
            foreach (Card::ORDER_NAME as $cardName => $nameValue)
            {
                array_push($card,new Card($cardName,$colorName));
            }
        }
        return $card;
    }




}