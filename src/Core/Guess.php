<?php

namespace App\Core;



/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
 {
  /**
   * @var $cards array a array of Cards
   */
  private $cards;

  /**
   * @var $selectedCard Card This is the card to be guessed by the player
   */
  private $selectedCard;

  private $withhelp;

  private $utilisateurCard;

    /**
     * Guess constructor.
     * @param array $cards
     * @param Card $selectedCard
     * @param $withhelp
     */
    public function __construct(array $cards)
    {
        $this->cards = $cards;
        $this->selectedCard = null;
        $this->withhelp = false;
        $this->utilisateurCard = null;
    }

    /**
     * @return array
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @param array $cards
     */
    public function setCards(array $cards): void
    {
        $this->cards = $cards;
    }

    /**
     * @return Card
     */
    public function getSelectedCard(): Card
    {
        return $this->selectedCard;
    }

    /**
     * @param Card $selectedCard
     */


    /**
     * @return bool
     */
    public function isWithhelp(): bool
    {
        return $this->withhelp;
    }

    /**
     * @param bool $withhelp
     */
    public function setWithhelp(bool $withhelp): void
    {
        $this->withhelp = $withhelp;
    }

    /**
     * @return mixed
     */
    public function getProcessUserProposall(): Card
    {
        return $this->utilisateurCard;
    }

    /**
     * @param mixed $utilisateurCard
     */



    public function Start() : void
    {
        $this->selectedCard = $this->cards[random_int(1,52)];
    }


    public function ProcessUserProposall(Card $utilisateurCard): void
    {
        $this->utilisateurCard = $utilisateurCard;
    }






}

