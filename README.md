# Guess What

**Projet realisé par:**

 DIarrassouba Bassémory et SAVOUNDIRAPANDIANE Muthuvel

### Objectif

- Mise au point de la logique applicative avec PHPUnit

- Notion de structure de données, recherche d’un élement dans une liste

- Analyse des actions du joueur (fonction du nombre de cartes, aides à la décision)

  

#### <u>**TP 1 et 2 réalisé individuellement de l'installation des prérequis à la prise de connaissance des TOODOs**</u>

####                                                                   **TP 1 de prise en main**

![image-20200912214601725](C:\laragon\www\PPE\guesswhat-master\images\image-20200912214601725.png)

Tout d'abord nous avons vérifié les prérequis de notre système et en tape sur la commande ligne php -version et puis nous avez installé composer. 

- [ ] `php cli`  doit être opérationnel. (en ligne de commande tester : `php -version`)

- [ ] `composer` doit être opérationnel. (en ligne de commande tester : `composer -V`)

#### Tester le bon fonctionnement de ce petit existant

##### Lancement des tests unitaires

À **la racine du projet** du projet, lancer la commande : `./bin/phpunit`

Le résultat attendu est :

```
kpu@kpu-x1:~/PhpstormProjects/GuessThat$ ./bin/phpunit
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing Project Test Suite
..RRR                                                               5 / 5 (100%)

Time: 51 ms, Memory: 6.00 MB

There were 3 risky tests: This test did not perform any assertions

1) App\Tests\Core\CardTest::testColor
2) App\Tests\Core\CardTest::testCmp
3) App\Tests\Core\CardTest::testToString

OK, but incomplete, skipped, or risky tests!
Tests: 5, Assertions: 3, Risky: 3.
```



### **TP2 implémentation des TODOs**

Voici les TODOS réalise par Bassémory

- création méthode testColor 

  ```
  public function testColor()
  {
      $card = new Card('As', 'trèfle');
      $this->assertEquals('trèfle', $card->getColor());
  
      $card = new Card('As', 'pique');
      $this->assertEquals('pique', $card->getColor());
  }
  ```

- Création méthode testCmp 

  ```
  public function testCmp()
  {
      //Nom et coulour sont meme
  
      $card = new Card('roi','carreau');
      $card1 = new Card('roi','carreau');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(0,$result);
  
      //Memes nom et couleurs different
      
      $card = new Card('roi','trèfle');
      $card1 = new Card('roi','pique');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(-1,$result);
  
  
      //Meme Couleur et Nom different
  
      $card = new Card('roi','pique');
      $card1 = new Card('reine','pique');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(+1,$result);
  
      //Nom et couleur different
  
      $card = new Card('roi','pique');
      $card1 = new Card('reine','carreau');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(+1,$result);
  }
  ```

  Source : \App\Core\Card

  ```
  public static function cmp(Card $o1, Card $o2) : int
  {
  
  
    $o1Name = strtolower($o1->getName());
    $o2Name = strtolower($o2->getName());
  
    $o1Color = strtolower($o1->getColor());
    $o2Color = strtolower($o2->getColor());
  
    //Meme Noms & Meme Couleur
  
  
    if(($o1Name == $o2Name) && ($o1Color == $o2Color)){
        return 0;
    }
  
    //Meme Noms & Differentes couleur
    else if(($o1Name == $o2Name) && ($o1Color != $o2Color)){
        if(Card::ORDER_COLOR[$o1Color] > Card::ORDER_COLOR[$o2Color]){
            return +1;
        }else{
            return -1;
        }
  
    //Different Noms & Memes Couleur
    } else if (($o1Name != $o2Name) && ($o1Color == $o2Color)){
        if(Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name]){
            return +1;
        }else{
            return -1;
        }
  
  
    //Different Noms & different Couleur
    }else{
        if(Card::ORDER_NAME[$o1Name] > Card::ORDER_NAME[$o2Name])
        {
            return +1;
        }else{
            return -1;
        }
    }
  }
  ```

- Création méthode testToString  

  ```
  public function testToString() : void
  {
  
     $card = new Card('roi','pique');
     $this->assertEquals("Name: roi Couleur: pique",$card);
  
  
  }
  ```

  

  

TP3 Conceptions de Test unitaire :