<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

  public function testName()
  {
    $card = new Card('As', 'Trèfle');
    $this->assertEquals('As', $card->getName());

    $card = new Card('2', 'Trèfle');
    $this->assertEquals('2', $card->getName());

  }

  public function testSort()
  {
      $cards = [1];
      // vérifie que la première carte est bien un As
      $card = new Card('As', 'Trèfle');
      $cards[0] = $card;
      $this->assertEquals('As', $cards[0]->getName());


      $card = new Card('2', 'Pique');
      $cards[1] = $card;
      $this->assertEquals('2', $cards[1]->getName());


      // trie le tableau $cards, en utilisant la fonction de comparaison Card::cmp
      // rem : la syntaxe n'est pas intuitive, on doit passer
      // le nom complet de la classe et le nom d'une méthode de comparaison.
      // (voir https://www.php.net/manual/fr/function.usort.php)
      usort($cards, array("App\Core\Card", "cmp"));

      // vérifie que le tableau $cards a bien été modifié par usort
      // dans la table ASCII, les chiffres sont placés avant les lettres de l'alphabet

  }

  public function testColor()
  {
      $card = new Card('As', 'trèfle');
      $this->assertEquals('trèfle', $card->getColor());

      $card = new Card('As', 'pique');
      $this->assertEquals('pique', $card->getColor());
  }

  public function testCmp()
  {
      //Nom et coulour sont meme

      $card = new Card('roi','carreau');
      $card1 = new Card('roi','carreau');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(0,$result);

      //Memes nom et couleurs different

      $card = new Card('roi','trèfle');
      $card1 = new Card('roi','pique');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(-1,$result);


      //Meme Couleur et Nom different

      $card = new Card('roi','pique');
      $card1 = new Card('reine','pique');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(+1,$result);

      //Nom et couleur different

      $card = new Card('roi','pique');
      $card1 = new Card('reine','carreau');
      $result = Card::cmp($card,$card1);
      $this->assertEquals(+1,$result);
  }

  public function testToString() : void
  {

     $card = new Card('roi','pique');
     $this->assertEquals("Name: roi Couleur: pique",$card);

  }


}
